/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Map;

public class MapSerializer {
	public static void save(DataOutputStream out, Field field, Object o) throws IOException, IllegalAccessException {
		ParameterizedType type = (ParameterizedType) field.getGenericType();
		Class<?> key = (Class<?>)type.getActualTypeArguments()[0];
		Class<?> value = (Class<?>)type.getActualTypeArguments()[1];
		out.writeInt(((Map)o).size());

		if (key == Byte.class) {
			Map<Byte, Object> m = (Map<Byte, Object>)o;
			for (Map.Entry<Byte, Object> e : m.entrySet()) {
				out.writeByte(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else if (key == Short.class) {
			Map<Short, Object> m = (Map<Short, Object>)o;
			for (Map.Entry<Short, Object> e : m.entrySet()) {
				out.writeShort(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else if (key == Integer.class) {
			Map<Integer, Object> m = (Map<Integer, Object>)o;
			for (Map.Entry<Integer, Object> e : m.entrySet()) {
				out.writeInt(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else if (key == Long.class) {
			Map<Long, Object> m = (Map<Long, Object>)o;
			for (Map.Entry<Long, Object> e : m.entrySet()) {
				out.writeLong(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else if (key == Float.class) {
			Map<Float, Object> m = (Map<Float, Object>)o;
			for (Map.Entry<Float, Object> e : m.entrySet()) {
				out.writeFloat(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else if (key == Double.class) {
			Map<Double, Object> m = (Map<Double, Object>)o;
			for (Map.Entry<Double, Object> e : m.entrySet()) {
				out.writeDouble(e.getKey());
				Serializer.save(out, e.getValue());
			}
		} else {
			Map<Object, Object> m = (Map<Object, Object>)o;
			for (Map.Entry<Object, Object> e : m.entrySet()) {
				Serializer.save(out, e.getKey());
				Serializer.save(out, e.getValue());
			}
		}
	}

	public static void load(DataInputStream in, Field field, Object o) throws IOException, IllegalAccessException {
		int size = in.readInt();
		ParameterizedType type = (ParameterizedType) field.getGenericType();
		Class<?> key = (Class<?>)type.getActualTypeArguments()[0];
		Class<?> value = (Class<?>)type.getActualTypeArguments()[1];

		if (key == Byte.class) {
			Map<Byte, Object> m = (Map<Byte, Object>)o;
			for (int i = 0; i < size; i++) {
				byte k = in.readByte();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else if (key == Short.class) {
			Map<Short, Object> m = (Map<Short, Object>)o;
			for (int i = 0; i < size; i++) {
				short k = in.readShort();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else if (key == Integer.class) {
			Map<Integer, Object> m = (Map<Integer, Object>)o;
			for (int i = 0; i < size; i++) {
				int k = in.readInt();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else if (key == Long.class) {
			Map<Long, Object> m = (Map<Long, Object>)o;
			for (int i = 0; i < size; i++) {
				long k = in.readLong();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else if (key == Float.class) {
			Map<Float, Object> m = (Map<Float, Object>)o;
			for (int i = 0; i < size; i++) {
				float k = in.readFloat();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else if (key == Double.class) {
			Map<Double, Object> m = (Map<Double, Object>)o;
			for (int i = 0; i < size; i++) {
				double k = in.readDouble();
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		} else {
			Map<Object, Object> m = (Map<Object, Object>)o;
			for (int i = 0; i < size; i++) {
				Object k = Serializer.load(in, null, key);
				Object v = Serializer.load(in, null, value);
				m.put(k, v);
			}
		}
	}
}
