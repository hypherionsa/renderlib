/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

public class CollectionSerializer {
	public static void save(DataOutputStream out, Field field, Object o) throws IOException {
		ParameterizedType type = (ParameterizedType) field.getGenericType();
		Class<?> cls = (Class<?>)type.getActualTypeArguments()[0];
		out.writeInt(((Collection)o).size());
		if (cls == Byte.class) {
			Collection<Byte> c = (Collection<Byte>)o;
			for (byte b : c) out.writeByte(b);
		} else if (cls == Short.class) {
			Collection<Short> c = (Collection<Short>)o;
			for (short b : c) out.writeShort(b);
		} else if (cls == Integer.class) {
			Collection<Integer> c = (Collection<Integer>)o;
			for (int b : c) out.writeInt(b);
		} else if (cls == Long.class) {
			Collection<Long> c = (Collection<Long>)o;
			for (long b : c) out.writeLong(b);
		} else if (cls == Float.class) {
			Collection<Float> c = (Collection<Float>)o;
			for (float b : c) out.writeFloat(b);
		} else if (cls == Double.class) {
			Collection<Double> c = (Collection<Double>)o;
			for (double b : c) out.writeDouble(b);
		} else {
			Collection c = (Collection)o;
			for (Object obj : c) {
				Serializer.save(out, obj);
			}
		}
	}

	public static void load(DataInputStream in, Field field, Object o) throws IOException {
		int size = in.readInt();
		ParameterizedType type = (ParameterizedType) field.getGenericType();
		Class<?> cls = (Class<?>)type.getActualTypeArguments()[0];
		if (cls == Byte.class) {
			Collection<Byte> c = (Collection<Byte>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readByte());
			}
		} else if (cls == Short.class) {
			Collection<Short> c = (Collection<Short>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readShort());
			}
		} else if (cls == Integer.class) {
			Collection<Integer> c = (Collection<Integer>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readInt());
			}
		} else if (cls == Long.class) {
			Collection<Long> c = (Collection<Long>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readLong());
			}
		} else if (cls == Float.class) {
			Collection<Float> c = (Collection<Float>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readFloat());
			}
		} else if (cls == Double.class) {
			Collection<Double> c = (Collection<Double>)o;
			for (int i = 0; i < size; i++) {
				c.add(in.readDouble());
			}
		} else {
			Collection c = (Collection)o;
			for (int i = 0; i < size; i++) {
				c.add(Serializer.load(in, null, cls));
			}
		}
	}
}
