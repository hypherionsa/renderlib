/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.render.ViewBox;
import friedrichlp.renderlib.threading.TaskManager;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class RenderLayer {
	protected static final int PROCESSING_BATCH_SIZE = 512;

	public final int id;
	public final ViewBox viewBox;
	private boolean hidden = false;
	private float renderDistanceOverride;
	private boolean overrideRenderDistance;
	private Int2ObjectArrayMap<ObjectContainer> objects = new Int2ObjectArrayMap<>();
	private Int2ObjectArrayMap<ObjectArrayList<RenderObject>> dynamicObjects = new Int2ObjectArrayMap<>();

	protected RenderLayer(int id, ViewBox viewBox) {
		this.id = id;
		this.viewBox = viewBox;
	}

	public RenderObject addRenderObject(ModelInfo mdl) {
		RenderObject obj = new RenderObject(mdl.model, this);

		synchronized (objects) {
			ObjectContainer oc = objects.get(mdl.model.id);
			if (oc == null) {
				oc = new ObjectContainer();
				objects.put(mdl.model.id, oc);
			}

			oc.objects.add(obj);
			oc.contentChanged = true;
		}

		return obj;
	}

	public void removeRenderObject(RenderObject obj) {
		synchronized (objects) {
			ObjectContainer oc = objects.get(obj.model.id);

			if (oc != null) {
				oc.objects.remove(obj);
				oc.contentChanged = true;
			}
		}
	}

	protected void setDynamic(RenderObject obj) {
		synchronized (objects) {
			ObjectContainer oc = objects.get(obj.model.id);
			oc.objects.remove(obj);
			oc.contentChanged = true;
		}

		synchronized (dynamicObjects) {
			ObjectArrayList<RenderObject> l = dynamicObjects.get(obj.model.id);
			if (l == null) {
				l = new ObjectArrayList<>();
				dynamicObjects.put(obj.model.id, l);
			}

			l.add(obj);
		}
	}

	protected void setDefault(RenderObject obj) {
		synchronized (dynamicObjects) {
			dynamicObjects.get(obj.model.id).remove(obj);
		}

		synchronized (objects) {
			ObjectContainer oc = objects.get(obj.model.id);
			oc.objects.add(obj);
			oc.contentChanged = true;
		}
	}

	protected void onObjectUpdate(RenderObject obj) {
		if (!obj.isDynamic) {
			synchronized (objects) {
				objects.get(obj.model.id).contentChanged = true;
			}
		}
	}

	protected void onObjectTransformUpdate(RenderObject obj) {
		if (!obj.isDynamic) {
			synchronized (objects) {
				objects.get(obj.model.id).transformChanged = true;
			}
		}
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public void overrideRenderDistance(float renderDistance) {
		renderDistanceOverride = renderDistance;
		overrideRenderDistance = true;
	}

	public void removeRenderDistanceOverride() {
		overrideRenderDistance = false;
	}

	public void clear() {
		synchronized (objects) {
			objects.clear();
		}
	}

	private int frame;
	protected void render() {
		if (hidden) return;
		frame++;

		float renderDist = overrideRenderDistance ? renderDistanceOverride : TrackingManager.getRenderDistance();

		synchronized (objects) {
			for (Int2ObjectMap.Entry<ObjectContainer> e : objects.int2ObjectEntrySet()) {
				Model model = ModelManager.getModel(e.getIntKey());
				ObjectContainer oc = e.getValue();

				boolean changed = false;
				boolean objectChanged = oc.contentChanged;

				if (objectChanged || frame % 5 == 0) {
					oc.count = 0;
					for (RenderObject obj : oc.objects) {
						boolean prev = obj.isVisible;
						try {
							obj.isVisible = false;
							if (obj.isHidden) continue;
							if (!viewBox.isObjectOnScreen(obj.position, obj.model.getHitbox())) continue;
							if (obj.position.distanceTo(TrackingManager.getCameraPos()) > renderDist) continue;
							obj.isVisible = true;
						} finally {
							if (obj.isVisible != prev) changed = true;
							if (obj.isVisible) oc.count++;
						}
					}
				}

				if (objectChanged || changed) oc.transform.updateData(oc.objects);

				model.render(oc.count, oc.transform);
			}
		}

		synchronized (dynamicObjects) {
			for (Int2ObjectMap.Entry<ObjectArrayList<RenderObject>> e : dynamicObjects.int2ObjectEntrySet()) {
				Model model = ModelManager.getModel(e.getIntKey());

				model.renderSetup(false);
				for (RenderObject obj : e.getValue()) {
					obj.isVisible = false;
					if (obj.isHidden) continue;
					if (!viewBox.isObjectOnScreen(obj.getPosition(), obj.model.getHitbox())) continue;
					if (obj.position.distanceTo(TrackingManager.getCameraPos()) > (overrideRenderDistance ? renderDistanceOverride : TrackingManager.getRenderDistance())) continue;
					obj.isVisible = true;
					obj.render();
				}
				model.renderCleanup();
			}
		}
	}

	protected void applyTransformUpdates() {
		synchronized (objects) {
			for (ObjectContainer oc : objects.values()) {
				if (oc.transformChanged) {
					oc.transformChanged = false;

					for (int j = 0; j < oc.objects.size(); j += PROCESSING_BATCH_SIZE) {
						TaskManager.runTaskAsync(new RenderObject.ProcessingBatch(oc.objects, j, Math.min(PROCESSING_BATCH_SIZE, oc.objects.size() - j)));
					}
				}
			}
		}
	}

	private static class ObjectContainer {
		private ObjectArrayList<RenderObject> objects = new ObjectArrayList<RenderObject>();
		private int count;
		private boolean contentChanged;
		private boolean transformChanged;

		private TransformStack transform = new TransformStack();
	}
}

