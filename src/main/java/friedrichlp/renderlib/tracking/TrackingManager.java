/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.RenderLibRegistry;
import friedrichlp.renderlib.animation.AnimationRunner;
import friedrichlp.renderlib.caching.CacheManager;
import friedrichlp.renderlib.library.GLValueType;
import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.math.Vector3;
import friedrichlp.renderlib.oglw.state.GLStateManager;
import friedrichlp.renderlib.render.GLState;
import friedrichlp.renderlib.render.ViewBox;
import friedrichlp.renderlib.threading.TaskManager;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.DynamicLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

public class TrackingManager {
	public static Matrix4f modelViewProjectionMatrix;
	public static Matrix4f modelViewMatrix;
	public static final RenderDebugInfo RENDER_DEBUG_INFO = new RenderDebugInfo();

	protected static GLState state;

	private static Map<Integer, RenderLayer> layers = new TreeMap<Integer, RenderLayer>(Integer::compareTo); // low to high
	private static Vector3 cameraPos = Vector3.ZERO();
	private static Vector3 lightPos = Vector3.ZERO();
	private static float renderDistance;
	private static Path TEMP_DIR;
	private static boolean initialized = false;
	private static int[] singleLayerId = new int[1];
	private static boolean isServer = false;

	public static Path getTempDir() {
		return TEMP_DIR;
	}

	public static void setCameraPos(Vector3 newCameraPos) {
		if (newCameraPos == null) {
			ConsoleLogger.warn("newCameraPos can't be null! Ignoring the input");
			return;
		}
		cameraPos.set(newCameraPos);
	}

	public static void setServer() {
		isServer = true;
	}

	public static boolean useGL() {
		return !isServer;
	}

	public static Vector3 getCameraPos() {
		return cameraPos;
	}

	public static void setRenderDistance(float newRenderDistance) {
		renderDistance = newRenderDistance;
	}

	public static Vector3 getLightPos() {
		return lightPos;
	}

	public static void setLightPos(Vector3 newLightPos) {
		if (newLightPos == null) {
			ConsoleLogger.warn("newLightPos can't be null! Ignoring the input");
			return;
		}
		lightPos = newLightPos;
	}

	public static float getRenderDistance() {
		return renderDistance;
	}

	public static RenderLayer getRenderLayer(int layerId) {
		return layers.get(layerId);
	}

	public static RenderLayer addRenderLayer(ViewBox viewBox) {
		return addRenderLayer(getAvailableRenderLayerId(), viewBox);
	}

	public static RenderLayer addRenderLayer(int id, ViewBox viewBox) {
		if (viewBox == null) {
			ConsoleLogger.warn("The input viewBox can't be null");
			return null;
		}
		if (layers.containsKey(id)) {
			ConsoleLogger.warn("The layerId %s already exists. You cannot override existing layers", id);
			return null;
		}

		RenderLayer layer = new RenderLayer(id, viewBox);
		layers.put(id, layer);
		ConsoleLogger.info("Registered new RenderLayer with ID %s", id);
		return layer;
	}

	public static void removeRenderLayer(RenderLayer layer) {
		layers.remove(layer.id);
	}

	public static int getAvailableRenderLayerId() {
		int idx = 0;
		while (true) {
			if (!layers.containsKey(idx)) return idx;
			idx++;
		}
	}

	public static void clear() {
		layers.values().forEach(RenderLayer::clear);
	}

	// prepares RenderLib for a VM shutdown
	public static void exit() {
		ModelManager.unloadAllModels();
		AnimationRunner.stopThread();
	}

	protected static void refreshModelViewProjection() {
		modelViewMatrix = RenderLibRegistry.Compatibility.MODEL_VIEW_PROVIDER.get();
		modelViewProjectionMatrix = RenderLibRegistry.Compatibility.MODEL_VIEW_PROJECTION_PROVIDER.get();
		modelViewProjectionMatrix.translate(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	}

	private static void prepareRender() {
		if (useGL()) state.save();
		TaskManager.runScheduledTasks();
		CacheManager.trySaveCaches();
		DynamicLoader.update();
		if (useGL()) state.restore();
	}

	/**
	 * This updates the internal state and all layers without rendering anything.
	 */
	public static void update() {
		if (!initialized) {
			initialize();
		}

		prepareRender();

		// Why should the server need to update the model matrices?
		// To save some CPU power we'll just ignore this on the server!
		if (useGL()) {
			for (RenderLayer l : layers.values()) {
				TaskManager.runTaskAsync(l::applyTransformUpdates);
			}
		}
	}

	/**
	 * Renders the single given RenderLayer to the screen.
	 */
	public static void render(RenderLayer layer) {
		singleLayerId[0] = layer.id;
		render(singleLayerId);
	}

	/**
	 * Either renders everything or only the specified layers.
	 *
	 * @param layerIds A list of layer ids to render or null if
	 *                 everything should be rendered.
	 */
	public static void render(int[] layerIds) {
		if (!initialized) {
			initialize();
		}

		prepareRender();

		if (!useGL()) {
			return;
		}

		state.save();
		GLStateManager.invalidateState();

		refreshModelViewProjection();
		Model.globalRenderSetup();

		if (layerIds == null) {
			for (Map.Entry<Integer, RenderLayer> e : layers.entrySet()) {
				long start = System.currentTimeMillis();
				e.getValue().render();
				RENDER_DEBUG_INFO.lastFrameLayerMs.put((int) e.getKey(), System.currentTimeMillis() - start);
			}
		} else {
			for (int id : layerIds) {
				RenderLayer layer = layers.get(id);
				if (layer == null) {
					ConsoleLogger.warn("Invalid layerId (%s)", id);
					continue;
				}

				long start = System.currentTimeMillis();
				layer.render();
				RENDER_DEBUG_INFO.lastFrameLayerMs.put((int) id, System.currentTimeMillis() - start);
			}
		}

		Model.globalRenderCleanup();

		if (layerIds == null) {
			for (RenderLayer l : layers.values()) {
				TaskManager.runTaskAsync(l::applyTransformUpdates);
			}
		} else {
			for (int id : layerIds) {
				RenderLayer layer = layers.get(id);
				if (layer == null) {
					ConsoleLogger.warn("Invalid layerId (%s)", id);
					continue;
				}

				TaskManager.runTaskAsync(layer::applyTransformUpdates);
			}
		}

		state.restore();
	}

	private static void initialize() {
		try {
			TEMP_DIR = Files.createTempDirectory(null);
			TEMP_DIR.toFile().deleteOnExit(); //ensure the garbage is cleaned up after the vm has been closed
		} catch (IOException ex) {
			ConsoleLogger.warn("Could not create a temp directory");
		}

		if (useGL()) {
			state = new GLState()
					.add(GLValueType.ARRAY_BUFFER)
					.add(GLValueType.UNIFORM_BUFFER)
					.add(GLValueType.TEXTURE_2D)
					.add(GLValueType.PIXEL_UNPACK_BUFFER)
					.add(GLValueType.PROGRAM)
					.add(GLValueType.CULL_FACE)
					.saveVertexAttribState();
		}

		initialized = true;
	}
}
