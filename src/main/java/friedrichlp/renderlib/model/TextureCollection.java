/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model;

import friedrichlp.renderlib.library.TextureType;
import friedrichlp.renderlib.oglw.GLBuffers;
import friedrichlp.renderlib.oglw.GLDrawSetup;
import friedrichlp.renderlib.render.Texture;
import friedrichlp.renderlib.render.VertexArray;
import friedrichlp.renderlib.threading.TaskManager;
import friedrichlp.renderlib.tracking.Model;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;

public class TextureCollection {
	private Int2ObjectArrayMap<Texture> textures = new Int2ObjectArrayMap<Texture>();
	private VertexArray mtlIdxBuf = new VertexArray(GLBuffers.Target.VERTEX_ARRAY); // per-vertex texture index
	public VertexArray textureMtlBuf = new VertexArray(GLBuffers.Target.UNIFORM_BUFFER); // mtl information
	private Model model;

	public TextureCollection(Model model) {
		this.model = model;
	}

	public void loadFromFile(ObjectArrayList<String> faceMTLs) {
		if (textures.size() != 0) {
			for (Texture tex : textures.values()) {
				TaskManager.scheduleTask(() -> {
					tex.unload();
				});
			}
			textures.clear();
		}

		for (TextureType t : TextureType.values()) {
			if (t.isActive(model.data.loadedTextureTypes)) {
				Texture tex = new Texture(model, faceMTLs, t);
				tex.loadFromFile();
				textures.put(t.ordinal(), tex);
			}
		}

		ByteBuffer mtlBuf = BufferUtils.createByteBuffer(model.data.vboSize * 3 * 2);
		Texture mapKd = textures.get(0); // we assume that every model has a map_kd loaded
		for (int[] group : model.groups.values()) {
			for (int face : group) {
				short mtlIdx = mapKd.getMtlIndex(faceMTLs.get(face));
				mtlBuf.putShort(mtlIdx);
				mtlBuf.putShort(mtlIdx);
				mtlBuf.putShort(mtlIdx);
			}
		}
		mtlBuf.flip();
		ByteBuffer texMtlBuf = mapKd.provideMtls();

		model.data.mtlIdxBuf = mtlBuf;
		model.data.textureMtlBuf = texMtlBuf;

		TaskManager.scheduleTask(() -> {
			mtlIdxBuf.set(mtlBuf);
			textureMtlBuf.set(texMtlBuf);
		});
	}

	public void loadFromCache() {
		for (TextureType t : TextureType.values()) {
			if (t.isActive(model.data.loadedTextureTypes)) {
				Texture tex = new Texture(model, t);
				tex.loadFromCache();
				textures.put(t.ordinal(), tex);
			}
		}

		mtlIdxBuf.set(model.data.mtlIdxBuf);
		textureMtlBuf.set(model.data.textureMtlBuf);
	}

	public boolean hasError() {
		for (Texture tex : textures.values()) {
			if (tex.hasError()) {
				return true;
			}
		}

		return false;
	}

	public void unload() {
		for (Texture tex : textures.values()) {
			tex.unload();
		}
		textures.clear();

		mtlIdxBuf.delete();
		textureMtlBuf.delete();
	}

	public void renderSetup() {
		mtlIdxBuf.bind();
		GLDrawSetup.vertexAttribPointer(4, 1, GLDrawSetup.Type.SHORT, false, 0, 0);

		for (Texture tex : textures.values()) {
			tex.renderSetup();
		}
	}

	public boolean isLoaded() {
		for (Texture tex : textures.values()) {
			if (!tex.isLoaded) {
				return false;
			}
		}

		return true;
	}
}
