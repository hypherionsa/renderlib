/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model.loader;

import friedrichlp.renderlib.model.MaterialCollection;
import friedrichlp.renderlib.tracking.Model;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OBJLoader3DS implements Loader {
	@Override
	public FloatArrayList loadModel(IFileContainer modelFile, Model model, Runnable fileLoadCallback) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(modelFile.getStream()));

		String currGroupName = "defaultName";
		IntArrayList currGroup = new IntArrayList();
		ObjectArrayList<String> mtlPaths = new ObjectArrayList<String>();
		String currentMaterial = null;

		IntArrayList faceVerts = new IntArrayList();
		ObjectArrayList<String> faceMTLs = new ObjectArrayList<String>();
		FloatArrayList verts = new FloatArrayList();
		FloatArrayList vertNorms = new FloatArrayList();
		FloatArrayList vertTex = new FloatArrayList();

		String[] arr = new String[3];
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("#")) continue;
			if (line.length() == 0) continue;
			String[] args = line.split(" ");
			switch (args[0]) {
				case "mtllib":
					mtlPaths.add(args[1]);
					break;
				case "usemtl":
					currentMaterial = args[1].intern();
					break;
				case "o":
				case "g":
					currGroupName = args[1].intern();
					if (currGroup.size() > 0) {
						model.groups.put(currGroupName, currGroup.elements());
					}
					currGroup = new IntArrayList();
					break;
				case "v":
					verts.add(Float.parseFloat(args[2]));
					verts.add(Float.parseFloat(args[3]));
					verts.add(Float.parseFloat(args[4]));
					break;
				case "vn":
					vertNorms.add(Float.parseFloat(args[1]));
					vertNorms.add(Float.parseFloat(args[2]));
					vertNorms.add(Float.parseFloat(args[3]));
					break;
				case "vt":
					vertTex.add(Float.parseFloat(args[1]));
					vertTex.add(Float.parseFloat(args[2]));
					break;
				case "f":
					int idx;
					if (args.length == 4) {
						arrCpy(arr, args, 1, 2, 3);
						loadFace(faceVerts, arr);
						idx = faceMTLs.size();
						faceMTLs.add(currentMaterial);
						currGroup.add(idx);
					} else if (args.length == 5) {
						arrCpy(arr, args, 1, 2, 3);
						loadFace(faceVerts, arr);
						idx = faceMTLs.size();
						faceMTLs.add(currentMaterial);
						currGroup.add(idx);

						arrCpy(arr, args, 3, 4, 1);
						loadFace(faceVerts, arr);
						idx = faceMTLs.size();
						faceMTLs.add(currentMaterial);
						currGroup.add(idx);
					} else {
						for (int i = 2; i < args.length - 1; i++) {
							arrCpy(arr, args, 1, i, i + 1);
							loadFace(faceVerts, arr);
							idx = faceMTLs.size();
							faceMTLs.add(currentMaterial);
							currGroup.add(idx);
						}
					}
					break;
				case "s":
				case "l":
					break;
				default:
					ConsoleLogger.debug("OBJ: ignored line '%s'", line);
					break;
			}
		}

		reader.close();

		if (mtlPaths.size() == 0) return verts;

		IFileContainer[] mtlFiles = new IFileContainer[mtlPaths.size()];
		for (int i = 0; i < mtlPaths.size(); i++) {
			IFileContainer fc = modelFile.getRelative(mtlPaths.get(i));
			mtlFiles[i] = fc;
			model.data.mtls.add(fc);
		}

		model.materials = MaterialCollection.loadFromMtls(mtlFiles);

		if (fileLoadCallback != null) {
			fileLoadCallback.run();
		}

		model.setData(verts, vertNorms, vertTex, faceVerts, faceMTLs);

		return verts;
	}

	private static void loadFace(IntArrayList faceVerts, String[] args) {
		for (int i = 0; i < args.length; i++) {
			String[] s = args[i].split("/");
			int[] point = new int[]{-1, -1, -1};
			for (int j = 0; j < s.length; j++) if (!s[j].equals("")) point[j] = Integer.parseInt(s[j]) - 1;
			for (int j : point) faceVerts.add(j);
		}
	}

	private void arrCpy(String[] dst, String[] src, int a, int b, int c) {
		dst[0] = src[a];
		dst[1] = src[b];
		dst[2] = src[c];
	}
}