/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model.loader;

import friedrichlp.renderlib.library.ModelType;
import friedrichlp.renderlib.tracking.Model;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import it.unimi.dsi.fastutil.floats.FloatArrayList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OBJLoader implements Loader {
	@Override
	public FloatArrayList loadModel(IFileContainer modelFile, Model model, Runnable fileLoadCallback) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(modelFile.getStream()));
		String firstLine = reader.readLine();
		reader.close();

		if (firstLine.contains("Blender")) {
			return ModelType.OBJ_BLENDER.loader.loadModel(modelFile, model, fileLoadCallback);
		} else if (firstLine.contains("3ds")) {
			return ModelType.OBJ_3DS.loader.loadModel(modelFile, model, fileLoadCallback);
		}

		ConsoleLogger.warn("Unable to determine obj model exporter for %s", modelFile.getPath());

		return null;
	}
}