/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.library;

import friedrichlp.renderlib.util.ConsoleLogger;
import org.lwjgl.opengl.*;

import java.util.function.BiConsumer;

public enum GLValueType {
	ARRAY_BUFFER((type, val) -> GL15.glBindBuffer(type, val), GL15.GL_ARRAY_BUFFER, GL15.GL_ARRAY_BUFFER_BINDING),
	UNIFORM_BUFFER((type, val) -> GL15.glBindBuffer(type, val), GL31.GL_UNIFORM_BUFFER, GL31.GL_UNIFORM_BUFFER_BINDING),
	TEXTURE_2D((type, val) -> GL11.glBindTexture(type, val), GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_BINDING_2D),
	PIXEL_UNPACK_BUFFER((type, val) -> GL15.glBindBuffer(type, val), GL21.GL_PIXEL_UNPACK_BUFFER, GL21.GL_PIXEL_UNPACK_BUFFER_BINDING),
	PROGRAM((type, val) -> GL20.glUseProgram(val), -1, GL20.GL_CURRENT_PROGRAM),

	CULL_FACE(true, GL11.GL_CULL_FACE)
	;

	BiConsumer<Integer, Integer> set;
	int setType;
	int getType;
	boolean isBool;
	GLValueType(BiConsumer<Integer, Integer> set, int setType, int getType) {
		this.set = set;
		this.setType = setType;
		this.getType = getType;
	}

	GLValueType(boolean isBool, int getType) {
		this.isBool = true;
		this.getType = getType;
	}

	public void set(int val) {
		if (isBool) {
			if (val == 0) {
				GL11.glDisable(getType);
			} else {
				GL11.glEnable(getType);
			}
		} else {
			set.accept(setType, val);
		}
	}

	public void set(boolean val) {
		if (!isBool) {
			ConsoleLogger.warn("Tried to set boolean value on non-boolean type");
			return;
		}

		if (val) {
			GL11.glEnable(getType);
		} else {
			GL11.glDisable(getType);
		}
	}

	public int get() {
		if (isBool) {
			return GL11.glGetBoolean(getType) ? 1 : 0;
		} else {
			return GL11.glGetInteger(getType);
		}
	}

	public int getType() {
		return setType;
	}
}
