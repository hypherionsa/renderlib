/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.threading;

import friedrichlp.renderlib.model.HitboxGenerator;
import friedrichlp.renderlib.tracking.Model;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import it.unimi.dsi.fastutil.floats.FloatArrayList;

import java.io.IOException;

public class ModelLoader implements Runnable {
	private Model model;
	private IFileContainer modelFile;

	public ModelLoader(Model model, IFileContainer modelFile) {
		this.model = model;
		this.modelFile = modelFile;
	}

	@Override
	public void run() {
		try {
			boolean changed = false;
			if (!changed && model.data.hasChanged()) {
				changed = true;
			}

			if (!changed) {
				model.data.readCache();
				TaskManager.scheduleTask(() -> model.loadFromCache());
			} else {
				model.data.awaitDataRefresh();

				model.getModelType().loader.loadModel(modelFile, model, () -> {
					model.data.updateHashedFiles();
					model.data.updateAdditionalCacheData();
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
			ConsoleLogger.warn("Unable to load model %s of type %s. The program will continue running but the model will stay empty.", modelFile.getPath(), model.getModelType().fileExtension);
		}
	}
}
