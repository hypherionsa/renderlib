/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.lang.reflect.Field;

public class HackUtil {
	private static Field object2ObjectArrayMap_Keys;
	private static Field object2ObjectArrayMap_Values;
	private static Field objectArrayList_Array;

	static {
		try {
			object2ObjectArrayMap_Keys = Object2ObjectArrayMap.class.getDeclaredField("key");
			object2ObjectArrayMap_Keys.setAccessible(true);
		} catch (Exception e) {
		}

		try {
			object2ObjectArrayMap_Values = Object2ObjectArrayMap.class.getDeclaredField("value");
			object2ObjectArrayMap_Values.setAccessible(true);
		} catch (Exception e) {
		}

		try {
			objectArrayList_Array = ObjectArrayList.class.getDeclaredField("a");
			objectArrayList_Array.setAccessible(true);
		} catch (Exception e) {
		}
	}

	public static Object[] getKeys(Object2ObjectArrayMap map) {
		try {
			return (Object[]) object2ObjectArrayMap_Keys.get(map);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object[] getValues(Object2ObjectArrayMap map) {
		try {
			return (Object[]) object2ObjectArrayMap_Values.get(map);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object[] getInternalArray(ObjectArrayList list) {
		try {
			return (Object[]) objectArrayList_Array.get(list);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
