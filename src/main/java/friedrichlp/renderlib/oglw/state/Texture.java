/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw.state;

import friedrichlp.renderlib.library.GLException;
import friedrichlp.renderlib.oglw.ExceptionHandler;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

public class Texture {
	private static int activeTextureSlot = 0;

	public static void bind(int target, int texture) {
		GL11.glBindTexture(target, texture);
	}

	public static void bind(int target, int texture, int slot) {
		if (activeTextureSlot != slot) {
			activeTextureSlot = slot;

			if (slot > 31) {
				ExceptionHandler.handle(GLException.ERROR, "Tried to bind texture to an invalid slot (%s)", slot);
				return;
			}

			GL13.glActiveTexture(GL13.GL_TEXTURE0 + slot);
		}

		GL11.glBindTexture(target, texture);
	}

	public static void invalidate() {
		activeTextureSlot = 0;
	}
}
