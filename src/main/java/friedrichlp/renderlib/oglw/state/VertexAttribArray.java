/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw.state;

import org.lwjgl.opengl.GL20;

public class VertexAttribArray {
	private static boolean[] vertexAttribArrays = new boolean[64];

	public static boolean isActive(int index) {
		return vertexAttribArrays[index];
	}

	public static void enable(int index) {
		vertexAttribArrays[index] = true;
		GL20.glEnableVertexAttribArray(index);
	}

	public static void disable(int index) {
		vertexAttribArrays[index] = false;
		GL20.glDisableVertexAttribArray(index);
	}

	public static void invalidate() {
		for (int i = 0; i < vertexAttribArrays.length; i++) {
			vertexAttribArrays[i] = false;
		}
	}
}
