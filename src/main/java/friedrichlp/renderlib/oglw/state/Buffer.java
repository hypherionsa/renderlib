/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw.state;

import it.unimi.dsi.fastutil.ints.Int2IntArrayMap;
import org.lwjgl.opengl.GL15;

public class Buffer {
	private static Int2IntArrayMap active = new Int2IntArrayMap();

	public static int getActive(int target) {
		return active.get(target);
	}

	public static void bind(int target, int buffer) {
		if (active.get(target) != buffer) {
			active.put(target, buffer);
			GL15.glBindBuffer(target, buffer);
		}
	}

	public static void invalidate() {
		active.clear();
	}
}
