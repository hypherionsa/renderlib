/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw;

import friedrichlp.renderlib.library.GLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL31;

public class GLDraw {
	public enum Mode {
		POINTS(GL11.GL_POINTS),
		LINES(GL11.GL_LINES),
		LINE_LOOP(GL11.GL_LINE_LOOP),
		LINE_STRIP(GL11.GL_LINE_STRIP),
		TRIANGLES(GL11.GL_TRIANGLES),
		TRIANGLE_STRIP(GL11.GL_TRIANGLE_STRIP),
		TRIANGLE_FAN(GL11.GL_TRIANGLE_FAN),
		QUADS(GL11.GL_QUADS),
		QUAD_STRIP(GL11.GL_QUAD_STRIP),
		POLYGON(GL11.GL_POLYGON)
		;

		private int val;
		Mode(int val) {
			this.val = val;
		}
	}


	public static void drawArraysInstanced(Mode mode, int first, int count, int instanceCount) {
		if (GLWrapperSettings.doChecks) {
			if (count < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "count (%s) was negative", count);
				return;
			} else if (instanceCount < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "instanceCount (%s) was negative", instanceCount);
				return;
			}
		}

		GL31.glDrawArraysInstanced(mode.val, first, count, instanceCount);
	}

	public static void drawArrays(Mode mode, int first, int count) {
		if (GLWrapperSettings.doChecks) {
			if (count < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "count (%s) was negative", count);
				return;
			}
		}

		GL11.glDrawArrays(mode.val, first, count);
	}
}
