/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.animation;

import friedrichlp.renderlib.util.ConsoleLogger;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class AnimationRunner extends Thread {
	private static final Thread THREAD;
	private static final long ANIMATION_INTERVAL = 20; // 20ms -> 1/50th of a second

	private static boolean running = true;
	private static boolean paused = false;
	private static ObjectArrayList<Animation> animations = new ObjectArrayList<Animation>();

	static {
		THREAD = new AnimationRunner();
		THREAD.start();
	}

	public AnimationRunner() {
		super("RenderLIB-AnimationThread");
	}

	public static void stopThread() {
		running = false;
	}

	/**
	 * The Thread is active by default. This only needs to be called if the Thread was stopped
	 */
	public static void startThread() {
		running = true;
		THREAD.start();
	}

	public static boolean isRunning() {
		return running;
	}

	public static void pauseThread() {
		paused = true;
	}

	public static void resumeThread() {
		paused = false;
	}

	public static boolean isPaused() {
		return paused;
	}

	protected static void register(Animation animation) {
		animations.add(animation);
	}

	protected static void remove(Animation animation) {
		animations.remove(animation);
	}

	@Override
	public void run() {
		while (running) {
			long startTime = System.currentTimeMillis();

			if (!paused) {
				for (Animation anim : animations) {
					anim.playAnimations(ANIMATION_INTERVAL / 1000f);
				}
			}

			long runningTime = System.currentTimeMillis() - startTime;
			long sleepTime = ANIMATION_INTERVAL - runningTime;
			if (sleepTime < 0) {
				ConsoleLogger.error("Too many animations playing. The thread can't keep up.");
				throw new RuntimeException("Animation thread overload"); // TODO: Maybe use an exception that makes more sense
			}
			try {
				sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}
}
