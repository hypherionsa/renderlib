/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.animation;

import friedrichlp.renderlib.math.Vector3;

public class Keyframe {
	public final float timeStamp;
	public final Vector3 loc;
	public final Vector3 rot;
	public final Vector3 scale;

	public Keyframe(float timeStamp, Vector3 loc, Vector3 rot, Vector3 scale) {
		this.timeStamp = timeStamp;
		this.loc = loc;
		this.rot = rot;
		this.scale = scale;
	}

	public static Keyframe lerp(Keyframe a, Keyframe b, float time) {
		float timeDiff = b.timeStamp - a.timeStamp;
		float timePos = time - a.timeStamp;
		float timePerc = timePos / timeDiff;
		return new Keyframe(time,
				Vector3.lerp(a.loc, b.loc, timePerc),
				Vector3.lerp(a.rot, b.rot, timePerc),
				Vector3.lerp(a.scale, b.scale, timePerc));
	}
}
