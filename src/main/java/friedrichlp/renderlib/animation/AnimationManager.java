/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.animation;

import friedrichlp.renderlib.tracking.RenderObject;
import friedrichlp.renderlib.util.ConsoleLogger;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;

import java.io.File;
import java.util.function.BiConsumer;

public class AnimationManager {
	private static Object2ObjectArrayMap<String, Animation> animations = new Object2ObjectArrayMap<String, Animation>();

	public static String registerAnimation(File animationFile) {
		Animation animation = new Animation(animationFile);
		String animationName = animationFile.getAbsolutePath();
		animations.put(animationName, animation);
		ConsoleLogger.debug("Registered animation %s", animationName);
		return animationName;
	}

	public static void unloadAllAnimations() {
		animations.clear();
	}

	public static void unloadModel(String animation) {
		animations.remove(animation);
	}

	public static Animation.AnimationInstance createAnimationInstance(String animationName, RenderObject link, BiConsumer<Keyframe, String> customAnimationHandler) {
		return animations.get(animationName).createInstance(link, customAnimationHandler);
	}
}
