/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.animation;

import friedrichlp.renderlib.animation.loader.AnimationLoader;
import friedrichlp.renderlib.math.Vector3;
import friedrichlp.renderlib.math.Vector3Triple;
import friedrichlp.renderlib.tracking.RenderObject;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IndexList;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.function.BiConsumer;

public class Animation {
	private Object2ObjectArrayMap<String, IndexList<Keyframe>> keyframes = new Object2ObjectArrayMap<String, IndexList<Keyframe>>();
	private Object2ObjectArrayMap<String, Vector3> origins = new Object2ObjectArrayMap<String, Vector3>();
	private ObjectArrayList<AnimationInstance> runningAnimations = new ObjectArrayList<AnimationInstance>();

	protected Animation(File animationFile) {
		AnimationRunner.register(this);

		try {
			AnimationLoader.loadAnimation(animationFile, this);
		} catch (IOException e) {
			ConsoleLogger.warn("Error when loading animation file %s", animationFile.getAbsolutePath());
		}
	}

	public void addKeyframe(String objName, Keyframe keyframe) {
		IndexList<Keyframe> list = keyframes.get(objName);
		if (list == null) {
			list = new IndexList<Keyframe>();
			keyframes.put(objName, list);
		}
		list.add(keyframe);
	}

	public void addKeyframes(String objName, IndexList<Keyframe> keyframes) {
		this.keyframes.put(objName, keyframes);
	}

	public void setObjectOrigin(String objName, Vector3 origin) {
		origins.put(objName, origin);
	}

	public AnimationInstance createInstance(RenderObject link, BiConsumer<Keyframe, String> customAnimationHandler) {
		return new AnimationInstance(this, link, customAnimationHandler);
	}

	// timeStep in seconds
	protected void playAnimations(float timeStep) {
		for (AnimationInstance anim : this.runningAnimations) {
			anim.step(timeStep);
		}
	}

	// unregisters animation and instances
	public void unload() {
		AnimationRunner.remove(this);
		runningAnimations.clear();
	}

	public static class AnimationInstance {
		private Animation parent;
		private RenderObject link;
		private BiConsumer<Keyframe, String> customAnimationHandler;
		private float playtime; //at which timestamp the animation playback currently is
		private Object2ObjectArrayMap<String, IndexList.Index> current;
		private Object2ObjectArrayMap<String, Vector3Triple> partOffsets = new Object2ObjectArrayMap<String, Vector3Triple>();

		private boolean playing;
		private boolean looping;
		private float speed = 1f;

		private AnimationInstance(Animation parent, RenderObject link, BiConsumer<Keyframe, String> customAnimationHandler) {
			this.parent = parent;
			current = new Object2ObjectArrayMap<String, IndexList.Index>(parent.keyframes.size());
			for (String s : parent.keyframes.keySet()) current.put(s, new IndexList.Index());

			this.link = link;
			this.customAnimationHandler = customAnimationHandler;

			for (Map.Entry<String, Vector3> entry : parent.origins.entrySet()) {
				link.setPartOrigin(entry.getKey(), entry.getValue().x, entry.getValue().y, entry.getValue().z);
			}
		}

		public void start() {
			playing = true;
			parent.runningAnimations.add(this);
		}

		public void stop() {
			playing = false;
			parent.runningAnimations.remove(this);
		}

		public boolean isPlaying() {
			return playing;
		}

		public void setLooping(boolean looping) {
			this.looping = looping;
		}

		public boolean isLooping() {
			return looping;
		}

		public void setPlaybackSpeed(float newSpeed) {
			speed = newSpeed;
		}

		public float getPlaybackSpeed() {
			return speed;
		}

		public void translatePart(String partName, float x, float y, float z) {
			Vector3Triple v = partOffsets.get(partName);
			if (v == null) {
				v = new Vector3Triple(Vector3.ZERO(), Vector3.ZERO(), Vector3.ONE());
				partOffsets.put(partName, v);
			}
			v.a.add(x, y, z);
		}

		public void rotatePart(String partName, float x, float y, float z) {
			Vector3Triple v = partOffsets.get(partName);
			if (v == null) {
				v = new Vector3Triple(Vector3.ZERO(), Vector3.ZERO(), Vector3.ONE());
				partOffsets.put(partName, v);
			}
			v.b.add(x, y, z);
		}

		public void scalePart(String partName, float x, float y, float z) {
			Vector3Triple v = partOffsets.get(partName);
			if (v == null) {
				v = new Vector3Triple(Vector3.ZERO(), Vector3.ZERO(), Vector3.ONE());
				partOffsets.put(partName, v);
			}
			v.c.add(x, y, z);
		}

		public void resetPartOffsets() {
			for (Vector3Triple v : partOffsets.values()) {
				v.a.setZero();
				v.b.setZero();
				v.c.setZero();
			}
		}

		private void step(float timeStep) {
			playtime += timeStep * speed;

			for (Map.Entry<String, IndexList.Index> entry : current.entrySet()) {
				IndexList<Keyframe> list = parent.keyframes.get(entry.getKey());
				Keyframe possibleNext = list.next(entry.getValue(), false);
				boolean incremented = false;
				if (possibleNext.timeStamp < playtime) {
					if (entry.getValue().index == 0) {
						if (looping) playtime = 0;
						else stop();
					}
					list.incrementIndex(entry.getValue());
					incremented = true;
				}
				Keyframe val = incremented ? possibleNext : list.get(entry.getValue());
				Keyframe nextVal = list.next(entry.getValue(), false);

				Keyframe state = Keyframe.lerp(val, nextVal, playtime);

				Vector3Triple v = partOffsets.get(entry.getKey());
				if (v != null) {
					state.loc.add(v.a);
					state.rot.add(v.b);
					state.scale.add(v.c);
				}

				if (link != null) {
					link.getPart(entry.getKey()).setTransform(state.loc, state.rot, state.scale);
				}
				if (customAnimationHandler != null)
					customAnimationHandler.accept(new Keyframe(timeStep, state.loc, state.rot, state.scale), entry.getKey());
			}
		}
	}
}
