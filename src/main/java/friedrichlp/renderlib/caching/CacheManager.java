/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.caching;

import friedrichlp.renderlib.RenderLibSettings;
import friedrichlp.renderlib.serialization.Serializer;
import friedrichlp.renderlib.util.ConsoleLogger;
import it.unimi.dsi.fastutil.longs.Long2ObjectArrayMap;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;

public class CacheManager {
	private static File cacheLocation;
	private static File cacheData;
	private static Long2ObjectArrayMap<Object> caches = new Long2ObjectArrayMap<Object>();
	private static boolean initialized = false;

	protected static boolean needsSave = false;

	private static void initialize() {
		initialized = true;

		try {
			cacheLocation = new File(RenderLibSettings.Caching.CACHE_LOCATION);
			cacheLocation.mkdirs();

			cacheData = getCacheFile("data.bin");
			if (!cacheData.createNewFile()) {
				try (DataInputStream in = new DataInputStream(new SnappyInputStream(new FileInputStream(cacheData)))) {
					String cacheVersion = in.readUTF();

					if (!cacheVersion.equals(RenderLibSettings.Caching.CACHE_VERSION)) {
						for (File file : cacheLocation.listFiles()) {
							file.delete();
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void link(Object o) {
		if (!initialized) {
			initialize();
		}

		if (o == null) {
			ConsoleLogger.warn("Link object was null");
			return;
		}

		if (!(o instanceof Cacheable)) {
			ConsoleLogger.warn("Tried to link non-cacheable object");
			return;
		}

		long nameHash = ((Cacheable)o).getNameHash();

		if (caches.containsKey(nameHash)) {
			ConsoleLogger.info("Tried to link cacheable twice");
			return;
		}

		caches.put(nameHash, o);

		File f = getCacheFile(((Cacheable)o).getNameHashString() + ".cache");
		if (f.exists()) {
			try (DataInputStream stream = new DataInputStream(new SnappyInputStream(new FileInputStream(f)))) {
				Serializer.load(stream, o, o.getClass());
				((Cacheable)o).onLoad(stream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static File getCacheFile(String name) {
		if (!initialized) {
			initialize();
		}

		return new File(cacheLocation, name);
	}

	public static void trySaveCaches() {
		if (!needsSave) return;
		needsSave = false;

		save();

		for (Object o : caches.values()) {
			File f = getCacheFile(((Cacheable)o).getNameHashString() + ".cache");

			try (DataOutputStream stream = new DataOutputStream(new SnappyOutputStream(new FileOutputStream(f)))) {
				Serializer.save(stream, o);
				((Cacheable)o).onSave(stream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void save() {
		try (DataOutputStream out = new DataOutputStream(new SnappyOutputStream(new FileOutputStream(cacheData)))) {
			out.writeUTF(RenderLibSettings.Caching.CACHE_VERSION);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
