/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.caching.util;

import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.StreamUtil;
import net.openhft.hashing.LongHashFunction;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class HashedFile {
	public IFileContainer file;
	public int size;
	public int blockSize;
	public long[] checksums;

	public HashedFile() {
	}

	public HashedFile(IFileContainer file, int blockSize) {
		this.file = file;
		try (InputStream in = file.getStream()) {
			size = StreamUtil.getSize(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.blockSize = blockSize;

		hash();
	}

	public HashedFile(HashedFile parent) {
		file = parent.file;
		try (InputStream in = file.getStream()) {
			size = StreamUtil.getSize(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		blockSize = parent.blockSize;

		hash();
	}

	private void hash() {
		try (ReadableByteChannel c = Channels.newChannel(file.getStream())) {
			ByteBuffer buf = BufferUtils.createByteBuffer(blockSize);
			int read = 0;
			int checksumIdx = 0;
			checksums = new long[(int) Math.ceil((double) size / blockSize)];
			while (read < size) {
				int readSize = Math.min(read + blockSize, size) - read;
				if (readSize != blockSize) buf = BufferUtils.createByteBuffer(readSize);
				c.read(buf);
				buf.flip();
				checksums[checksumIdx++] = LongHashFunction.wy_3().hashBytes(buf);
				read += blockSize;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isEqualTo(HashedFile other) {
		if (size != other.size || checksums.length != other.checksums.length) return false;
		for (int i = 0; i < checksums.length; i++) {
			if (checksums[i] != other.checksums[i]) return false;
		}
		return true;
	}
}
