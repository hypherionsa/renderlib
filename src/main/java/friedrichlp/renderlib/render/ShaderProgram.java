/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.render;

import friedrichlp.renderlib.RenderLibRegistry;
import friedrichlp.renderlib.library.RenderEffect;
import friedrichlp.renderlib.library.RenderProperty;
import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.math.Vector3;
import it.unimi.dsi.fastutil.longs.Long2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import org.lwjgl.opengl.*;

import java.nio.FloatBuffer;
import java.util.Scanner;

public class ShaderProgram {
	private Long2ObjectArrayMap<SubShader> shaders = new Long2ObjectArrayMap<SubShader>();
	private String vertShader;
	private String fragShader;

	public ShaderProgram(String vert, String frag) {
		vertShader = readShader(vert);
		fragShader = readShader(frag);
	}

	public SubShader get(int effects, int properties) {
		long code = (effects << 32) | properties;
		SubShader s = shaders.get(code);
		if (s == null) {
			s = new SubShader(effects, properties, vertShader, fragShader);
			shaders.put(code, s);
		}
		return s;
	}

	private static String getShaderLog(int id) {
		return GL20.glGetShaderInfoLog(id, GL20.glGetShaderi(id, GL20.GL_INFO_LOG_LENGTH));
	}

	private static String getProgramLog(int id) {
		return GL20.glGetProgramInfoLog(id, GL20.glGetProgrami(id, GL20.GL_INFO_LOG_LENGTH));
	}

	private static String readShader(String filename) {
		try (Scanner reader = new Scanner(ShaderProgram.class.getResourceAsStream(filename))) {
			StringBuilder text = new StringBuilder();
			while (reader.hasNextLine()) {
				text.append(reader.nextLine());
				text.append('\n');
			}
			return text.toString();
		}
	}

	public static class SubShader {
		private int id;
		private Object2ObjectArrayMap<String, Integer> cachedLocations = new Object2ObjectArrayMap<String, Integer>();

		private SubShader(int effects, int properties, String vertShader, String fragShader) {
			int vertId = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
			int fragId = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);

			GL20.glShaderSource(vertId, modifyShader(effects, properties, vertShader));
			GL20.glCompileShader(vertId);
			if (GL20.glGetShaderi(vertId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
				throw new RuntimeException("Error creating shader: " + getShaderLog(vertId));

			GL20.glShaderSource(fragId, modifyShader(effects, properties, fragShader));
			GL20.glCompileShader(fragId);
			if (GL20.glGetShaderi(fragId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
				throw new RuntimeException("Error creating shader: " + getShaderLog(fragId));

			id = GL20.glCreateProgram();
			GL20.glAttachShader(id, vertId);
			GL20.glAttachShader(id, fragId);
			GL20.glLinkProgram(id);
			if (GL20.glGetProgrami(id, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
				throw new RuntimeException("Error creating shader: " + getProgramLog(id));
			}
			GL20.glValidateProgram(id);
			if (GL20.glGetProgrami(id, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
				throw new RuntimeException("Error creating shader: " + getProgramLog(id));
			}

			bind();
			setInteger("bumpMap", 1);
			setInteger("metallicMap", 2);
		}

		private String modifyShader(int effects, int properties, String shaderCode) {
			String defineReplacement = "";

			for (RenderEffect e : RenderEffect.values()) {
				if (e.isActive(effects)) {
					defineReplacement += "#define " + e.name() + "\n";
				}
			}
			for (RenderProperty p : RenderProperty.values()) {
				if (p.isActive(properties)) {
					defineReplacement += "#define " + p.name() + "\n";
				}
			}

			if (RenderProperty.METALLIC_MAP.isActive(properties)) {
				defineReplacement += "#define PBR\n";
			}

			return ShaderPreprocessor.process(shaderCode.replace("##defines##", defineReplacement));
		}

		private int getUniformLocation(String name) {
			if (cachedLocations.containsKey(name)) return cachedLocations.get(name);

			int location = ARBShaderObjects.glGetUniformLocationARB(id, name);
			cachedLocations.put(name, location);
			return location;
		}

		private int getUniformBlockLocation(String name){
			if (cachedLocations.containsKey(name)) return cachedLocations.get(name);

			int location = GL31.glGetUniformBlockIndex(id, name);
			cachedLocations.put(name, location);
			return location;
		}

		public void bind() {
			GL20.glUseProgram(id);
		}

		public void setFloat(String name, float... params) {
			switch (params.length) {
				case 1:
					GL20.glUniform1f(getUniformLocation(name), params[0]);
					break;
				case 3:
					GL20.glUniform3f(getUniformLocation(name), params[0], params[1], params[2]);
					break;
			}
		}

		public void setInteger(String name, int param) {
			GL20.glUniform1i(getUniformLocation(name), param);
		}

		public void setVector(String name, Vector3 v) {
			GL20.glUniform3f(getUniformLocation(name), v.x, v.y, v.z);
		}

		public void setMatrix(String name, Matrix4f mat) {
			RenderLibRegistry.Compatibility.GL_SET_MATRIX_PARAMETER.accept(id, getUniformLocation(name), mat);
		}

		public void setMatrix(String name, FloatBuffer buf) {
			RenderLibRegistry.Compatibility.GL_SET_MATRIX_PARAMETER_FLOAT_BUFFER.accept(id, getUniformLocation(name), buf);
		}

		public void setBoolean(String name, boolean param) {
			setInteger(name, param ? 1 : 0);
		}

		public void bindUniformBlock(int blockId, String name, int loc) {
			GL31.glUniformBlockBinding(id, getUniformBlockLocation(name), loc);
			GL30.glBindBufferBase(GL31.GL_UNIFORM_BUFFER, loc, blockId);
		}

		public int getId() {
			return id;
		}
	}
}