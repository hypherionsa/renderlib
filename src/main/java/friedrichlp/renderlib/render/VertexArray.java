/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.render;

import friedrichlp.renderlib.oglw.GLBuffers;
import friedrichlp.renderlib.threading.TaskManager;
import friedrichlp.renderlib.util.UnsafeUtil;
import sun.nio.ch.DirectBuffer;

import java.nio.ByteBuffer;

public class VertexArray {
	private int id = -1;
	private int size = 0;
	private ByteBuffer access;
	private GLBuffers.Target type;

	public VertexArray(GLBuffers.Target type) {
		this.type = type;
	}

	public void set(ByteBuffer buf) {
		if (id != -1) delete();
		id = GLBuffers.genBuffer();
		bind();
		size = buf.capacity();
		GLBuffers.setBufferData(type, buf, GLBuffers.Usage.STATIC_DRAW);
	}

	public void set(ByteBuffer buf, Runnable onFinishCopy) {
		if (id != -1) delete();
		id = GLBuffers.genBuffer();
		bind();
		size = buf.capacity();
		GLBuffers.setBufferData(type, size, GLBuffers.Usage.STATIC_DRAW);
		map();

		TaskManager.runTaskAsync(() -> {
			UnsafeUtil.memcpy(((DirectBuffer) buf).address(), ((DirectBuffer) access).address(), size);

			TaskManager.scheduleTask(() -> {
				unmap();
				onFinishCopy.run();
			});
		});
	}

	private void map() {
		access = GLBuffers.mapBuffer(type, GLBuffers.Access.WRITE_ONLY);
	}

	private void unmap() {
		bind();
		GLBuffers.unmapBuffer(type); // sometimes causes GL_INVALID_OPERATION when optifine is loaded (for Minecraft)
		access = null;
	}

	public void bind() {
		GLBuffers.bind(type, id);
	}

	public void delete() {
		GLBuffers.deleteBuffer(id);
		id = -1;
		size = 0;
	}

	public int size() {
		return size;
	}

	public int getId() {
		return id;
	}
}
