/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.render;

import friedrichlp.renderlib.library.TextureType;
import friedrichlp.renderlib.math.Vector4;
import friedrichlp.renderlib.model.Material;
import friedrichlp.renderlib.model.MaterialCollection;
import friedrichlp.renderlib.model.ModelData;
import friedrichlp.renderlib.oglw.GLBuffers;
import friedrichlp.renderlib.oglw.GLTextures;
import friedrichlp.renderlib.threading.TaskManager;
import friedrichlp.renderlib.tracking.Model;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.UnsafeUtil;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.BufferUtils;
import sun.nio.ch.DirectBuffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

public class Texture {
	public TextureType type;
	public int size;
	public boolean isLoaded;

	private int textureId = -1;
	private Object2ObjectArrayMap<String, OffsetColor> offsets = new Object2ObjectArrayMap<String, OffsetColor>();
	private Object2ObjectArrayMap<String, String> remappedMtls = new Object2ObjectArrayMap<String, String>();
	private Object2ObjectArrayMap<String, ImageEntry> images = new Object2ObjectArrayMap<String, ImageEntry>();
	private boolean error = false;
	private Model model;

	public Texture(Model model, TextureType type) {
		this.model = model;
		this.type = type;
	}

	public Texture(Model model, ObjectArrayList<String> faceMTLs, TextureType type) {
		this(model, type);

		MaterialCollection mc = model.materials;

		for (String groupName : model.groups.keySet()) {
			int[] quads = model.groups.get(groupName);
			for (int face : quads) {
				String mtlName = faceMTLs.get(face);
				Material material = mc.materials.get(mtlName);
				if (material == null) continue;

				IFileContainer tex = material.textures.get(type);
				if (tex != null) {
					String key = tex.toString();
					if (!images.containsKey(key)) {
						images.put(key, new ImageEntry(tex, mtlName));
					} else {
						remappedMtls.put(mtlName, images.get(key).name);
					}
				} else if (material.Kd != null) {
					if (!offsets.containsKey(mtlName)) {
						float r = Math.max(0, material.Kd[0] - model.properties.darken);
						float g = Math.max(0, material.Kd[1] - model.properties.darken);
						float b = Math.max(0, material.Kd[2] - model.properties.darken);
						float a = material.Kd[3];
						offsets.put(mtlName, new OffsetColor(new Vector4(r, g, b, a), true, 0));
					}
				}
			}
		}

		for (ImageEntry e : images.values()) {
			if (e.error) {
				ConsoleLogger.warn("An error occured while loading a texture into the texture sheet");
				error = true;
				return;
			}
		}

		List<Map.Entry<String, ImageEntry>> entries = new ArrayList<Map.Entry<String, ImageEntry>>(this.images.entrySet());
		entries.sort((a, b) -> ((Integer) b.getValue().getImageWidth()).compareTo(a.getValue().getImageWidth()));
		images = new Object2ObjectArrayMap<String, ImageEntry>(entries.size());
		for (Map.Entry<String, ImageEntry> e : entries) {
			images.put(e.getKey(), e.getValue());
		}

		int pixelCount = 0;
		for (ImageEntry e : images.values()) pixelCount += Math.pow(round2(e.size, false), 2);
		size = round2(pixelCount, true);

		int biggestKey = size;
		TreeMap<Integer, Queue<TextureSpace>> textureSpaces = new TreeMap<Integer, Queue<TextureSpace>>();
		for (int i = 0; i < 15; i++) textureSpaces.put((int) Math.pow(2, i), new LinkedList<TextureSpace>());
		textureSpaces.get(size).add(new TextureSpace(0, 0, size));

		for (ImageEntry image : images.values()) {
			int correctedSize = round2(image.size, false);
			Queue<TextureSpace> entry = textureSpaces.get(correctedSize);
			if (entry.isEmpty()) {
				int nextSize = biggestKey;
				while (entry.isEmpty()) {
					int newSize = nextSize / 2;
					Queue<TextureSpace> texSpaces = textureSpaces.get(nextSize);
					Queue<TextureSpace> newTexSpaces = textureSpaces.get(newSize);
					TextureSpace texSpace = texSpaces.poll();
					newTexSpaces.add(new TextureSpace(texSpace.x, texSpace.y, newSize));
					newTexSpaces.add(new TextureSpace(texSpace.x + newSize, texSpace.y, newSize));
					newTexSpaces.add(new TextureSpace(texSpace.x, texSpace.y + newSize, newSize));
					newTexSpaces.add(new TextureSpace(texSpace.x + newSize, texSpace.y + newSize, newSize));

					if (texSpaces.isEmpty()) biggestKey = newSize;
					nextSize = newSize;
				}
			}

			TextureSpace texSpace = entry.poll();
			offsets.put(image.name, new OffsetColor(new Vector4(texSpace.x, texSpace.y, image.getImageWidth(), image.getImageHeight()).div(size), false, texSpace.size));

			image.unload();
		}
	}

	public void load() {

	}

	public void loadFromFile() {
		List<Runnable> tasks = new ArrayList<Runnable>(images.size());

		for (ImageEntry image : images.values()) {
			image.load();
			OffsetColor off = offsets.get(image.name);

			int[] pixels = new int[image.getImageWidth() * image.getImageHeight()];
			image.image.getRGB(0, 0, image.getImageWidth(), image.getImageHeight(), pixels, 0, image.getImageWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(off.squareSize * 4);
			for (int y = image.getImageHeight() - 1; y >= 0; y--) {
				for (int x = 0; x < image.getImageWidth(); x++) {
					int pixel = pixels[y * image.getImageWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) ((pixel >> 0) & 0xFF));
					buffer.put((byte) ((pixel >> 24) & 0xFF));
				}
			}
			buffer.flip();
			tasks.add(() -> {
				GLTextures.setSubImage2D(GLTextures.Target.TEXTURE_2D, 0, (int)(off.v.x * size), (int)(off.v.y * size),
						(int)(off.v.z * size), (int)(off.v.w * size), GLTextures.Format.RGBA, GLTextures.Type.UNSIGNED_BYTE, buffer);
			});

			image.unload();
		}

		TaskManager.scheduleTask(() -> {
			if (textureId != -1) GLTextures.deleteTexture(textureId);
			allocateTexture(size);

			tasks.forEach(Runnable::run);

			ByteBuffer textureBuf = BufferUtils.createByteBuffer(size * size * 4);
			GLTextures.getImage(GLTextures.Target.TEXTURE_2D, 0, GLTextures.Format.RGBA, GLTextures.Type.UNSIGNED_BYTE, textureBuf);
			ModelData.CachedTexture cacheTex = new ModelData.CachedTexture();
			cacheTex.buf = textureBuf;
			cacheTex.size = size;
			model.data.textures.put(type.ordinal(), cacheTex);

			isLoaded = true;
		});

	}

	public void loadFromCache() {
		ModelData.CachedTexture cacheTex = model.data.textures.get(type.ordinal());

		final int pbo = GLBuffers.genBuffer();
		GLBuffers.bind(GLBuffers.Target.PIXEL_UNPACK_BUFFER, pbo);
		GLBuffers.setBufferData(GLBuffers.Target.PIXEL_UNPACK_BUFFER, cacheTex.buf.capacity(), GLBuffers.Usage.STATIC_READ);
		final ByteBuffer pboAccess = GLBuffers.mapBuffer(GLBuffers.Target.PIXEL_UNPACK_BUFFER, GLBuffers.Access.WRITE_ONLY);
		GLBuffers.bind(GLBuffers.Target.PIXEL_UNPACK_BUFFER, 0);

		TaskManager.runTaskAsync(() -> {
			UnsafeUtil.memcpy(((DirectBuffer) cacheTex.buf).address(), ((DirectBuffer) pboAccess).address(), cacheTex.buf.capacity());

			TaskManager.scheduleTask(() -> {
				if (textureId != -1) GLTextures.deleteTexture(textureId);
				allocateTexture(cacheTex.size);

				GLBuffers.bind(GLBuffers.Target.PIXEL_UNPACK_BUFFER, pbo);
				GLBuffers.unmapBuffer(GLBuffers.Target.PIXEL_UNPACK_BUFFER);

				GLTextures.setSubImage2D(GLTextures.Target.TEXTURE_2D, 0, 0, 0, cacheTex.size, cacheTex.size, GLTextures.Format.RGBA, GLTextures.Type.UNSIGNED_BYTE, 0);

				GLBuffers.bind(GLBuffers.Target.PIXEL_UNPACK_BUFFER, 0);
				GLBuffers.deleteBuffer(pbo);

				cacheTex.buf = null;
				isLoaded = true;
				model.finishLoading();
			});
		});
	}

	public short getMtlIndex(String mtl) {
		mtl = remappedMtls.getOrDefault(mtl, mtl);
		short idx = 0;
		for (Map.Entry<String, OffsetColor> e : offsets.entrySet()) {
			if (e.getKey().equals(mtl)) {
				if (e.getValue().isColor) idx += 32768;
				break;
			}
			idx++;
		}
		return idx;
	}

	public ByteBuffer provideMtls() {
		ByteBuffer buf = BufferUtils.createByteBuffer(512 * 4 * 4);
		for (OffsetColor oc : offsets.values()) {
			buf.putFloat(oc.v.x);
			buf.putFloat(oc.v.y);
			buf.putFloat(oc.v.z);
			buf.putFloat(oc.v.w);
		}
		buf.flip();
		return buf;
	}

	public void unload() {
		GLTextures.deleteTexture(textureId);
		textureId = -1;
	}

	public void renderSetup() {
		GLTextures.bind(GLTextures.Target.TEXTURE_2D, textureId, type.ordinal());
	}

	private int round2(int i, boolean root) {
		return (int) Math.pow(2, Math.ceil(Math.log(root ? Math.sqrt(i) : i) / Math.log(2)));
	}

	private void allocateTexture(int size) {
		textureId = GLTextures.genTexture();
		GLTextures.bind(GLTextures.Target.TEXTURE_2D, textureId);

		GLTextures.setParameter(GLTextures.Target.TEXTURE_2D, GLTextures.ParamName.TEXTURE_MIN_FILTER, GLTextures.ParamValue.NEAREST);
		GLTextures.setParameter(GLTextures.Target.TEXTURE_2D, GLTextures.ParamName.TEXTURE_MAG_FILTER, GLTextures.ParamValue.NEAREST);
		GLTextures.setParameter(GLTextures.Target.TEXTURE_2D, GLTextures.ParamName.TEXTURE_WRAP_S, GLTextures.ParamValue.CLAMP_TO_EDGE);
		GLTextures.setParameter(GLTextures.Target.TEXTURE_2D, GLTextures.ParamName.TEXTURE_WRAP_T, GLTextures.ParamValue.CLAMP_TO_EDGE);

		GLTextures.setImage2D(GLTextures.Target.TEXTURE_2D, 0, GLTextures.InternalFormat.SRGB_ALPHA, size, size, GLTextures.Format.BGRA, GLTextures.Type.UNSIGNED_BYTE, null);
	}

	public boolean hasError() {
		return error;
	}

	private static class TextureSpace {
		private int x;
		private int y;
		private int size;

		private TextureSpace(int x, int y, int size) {
			this.x = x;
			this.y = y;
			this.size = size;
		}
	}

	private static class ImageEntry {
		private IFileContainer file;
		private BufferedImage image;
		private String name; //name of the mtl entry
		private int size;
		private boolean error = false;

		private ImageEntry(IFileContainer file, String name) {
			this.file = file;
			this.name = name;
			this.load();
		}

		private void load() {
			try {
				image = ImageIO.read(file.getStream());
			} catch (IOException e) {
				ConsoleLogger.warn("Error loading texture file %s", file.toString());
				error = true;
			}
			size = getImageWidth() > getImageHeight() ? getImageWidth() : getImageHeight();
		}

		private void unload() {
			image = null;
		}

		private int getImageWidth() {
			return image.getWidth();
		}

		private int getImageHeight() {
			return image.getHeight();
		}
	}

	private static class OffsetColor {
		private Vector4 v;
		private boolean isColor;
		private int squareSize;

		private OffsetColor(Vector4 v, boolean isColor, int size) {
			this.v = v;
			this.isColor = isColor;
			this.squareSize = size * size;
		}
	}
}
