/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib;

import friedrichlp.renderlib.caching.ICacheData;
import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.StandardFileContainer;
import friedrichlp.renderlib.util.TriConsumer;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class RenderLibRegistry {
	public static class Compatibility {
		public static TriConsumer<Integer, Integer, Matrix4f> GL_SET_MATRIX_PARAMETER = (program, loc, mat) -> ARBShaderObjects.glUniformMatrix4fvARB(loc, false, mat.f);
		public static TriConsumer<Integer, Integer, FloatBuffer> GL_SET_MATRIX_PARAMETER_FLOAT_BUFFER = (program, loc, buf) -> ARBShaderObjects.glUniformMatrix4fvARB(loc, false, buf);
		public static BiFunction<Integer, Integer, Integer> GL_GET_VERTEX_ATTRIB = (index, name) -> GL20.glGetVertexAttribi(index, name);

		public static Supplier<Matrix4f> MODEL_VIEW_PROJECTION_PROVIDER = () -> {
			float[] modelView = new float[16];
			float[] projection = new float[16];
			GL11.glGetFloatv(GL11.GL_MODELVIEW, modelView);
			GL11.glGetFloatv(GL11.GL_PROJECTION, projection);
			return new Matrix4f(modelView).multiply(new Matrix4f(projection));
		};

		public static Supplier<Matrix4f> MODEL_VIEW_PROVIDER = () -> {
			float[] modelView = new float[16];
			GL11.glGetFloatv(GL11.GL_MODELVIEW, modelView);
			return new Matrix4f(modelView);
		};
	}

	public static class FileContainer {
		private static List<Class<? extends IFileContainer>> entries = new ArrayList<Class<? extends IFileContainer>>();

		static {
			register(StandardFileContainer.class);
		}

		public static void register(Class<? extends IFileContainer> clazz) {
			entries.add(clazz);
		}

		public static int indexOf(Class<? extends IFileContainer> clazz) {
			int idx = entries.indexOf(clazz);
			if (idx == -1) ConsoleLogger.error("The StandardFileContainer class %s has not been registered!", clazz.getName());
			return idx;
		}

		public static Class<? extends IFileContainer> get(int idx) {
			return entries.get(idx);
		}
	}
}
