/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.math;

import friedrichlp.renderlib.caching.util.SerializeField;

public class HitBox3 {
	@SerializeField
	public float minX;
	@SerializeField
	public float minY;
	@SerializeField
	public float maxX;
	@SerializeField
	public float maxY;
	@SerializeField
	public float minZ;
	@SerializeField
	public float maxZ;

	public HitBox3() {}

	public HitBox3(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		this.minZ = minZ;
		this.maxZ = maxZ;
	}

	/**
	 * Extends this HitBox so that it is large enough to include box.
	 */
	public void combine(HitBox3 box) {
		minX = Math.min(minX, box.minX);
		minY = Math.min(minY, box.minY);
		minZ = Math.min(minZ, box.minZ);
		maxX = Math.max(maxX, box.maxX);
		maxY = Math.max(maxY, box.maxY);
		maxZ = Math.max(maxZ, box.maxZ);
	}

	public Vector3 center(Vector3 v) {
		v.x = minX + (maxX - minX) * 0.5f;
		v.y = minY + (maxY - minY) * 0.5f;
		v.z = minZ + (maxZ - minZ) * 0.5f;
		return v;
	}

	public float xDiff() {
		return maxX - minX;
	}

	public float yDiff() {
		return maxY - minY;
	}

	public float zDiff() {
		return maxZ - minZ;
	}
}
