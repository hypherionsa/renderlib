/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.math;

import friedrichlp.renderlib.util.Temporary;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

public class Matrix4f {
	public static final Matrix4f IDENTITY = new Matrix4f();
	public static final FloatBuffer IDENTITY_BUFFER = IDENTITY.getAsBuffer();

	private static float[] identityArray = new float[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
	private static final Temporary<Matrix4f> tmp = new Temporary<Matrix4f>(Matrix4f.class, 10000);

	public static Matrix4f TRANSLATE(float x, float y, float z) {
		Matrix4f m = TEMPORARY();
		m.f[12] = x;
		m.f[13] = y;
		m.f[14] = z;
		return m;
	}

	public static Matrix4f SCALE(float x, float y, float z) {
		Matrix4f m = TEMPORARY();
		m.f[0] = x;
		m.f[5] = y;
		m.f[10] = z;
		return m;
	}

	public static Matrix4f TEMPORARY() {
		return tmp.get().setIdentity();
	}

	public static Matrix4f TEMPORARY(float[] f) {
		return tmp.get().set(f);
	}


	public final float[] f = new float[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};

	public Matrix4f() {
	}

	public Matrix4f(float[] f) {
		System.arraycopy(f, 0, this.f, 0, 16);
	}

	public Matrix4f(FloatBuffer f) {
		for (int i = 0; i < 16; i++) this.f[i] = f.get(i);
	}

	public FloatBuffer getAsBuffer() {
		FloatBuffer buf = BufferUtils.createFloatBuffer(16);
		for (int i = 0; i < 16; i++) buf.put(this.f[i]);
		buf.flip();
		return buf;
	}

	public void writeToBuffer(FloatBuffer buf) {
		for (int i = 0; i < 16; i++) buf.put(f[i]);
		buf.flip();
	}

	public Matrix4f setIdentity() {
		System.arraycopy(identityArray, 0, f, 0, 16);
		return this;
	}

	public Matrix4f set(float[] f) {
		System.arraycopy(f, 0, this.f, 0, 16);
		return this;
	}

	public Matrix4f translate(float x, float y, float z) {
		f[12] += f[0] * x + f[4] * y + f[8] * z;
		f[13] += f[1] * x + f[5] * y + f[9] * z;
		f[14] += f[2] * x + f[6] * y + f[10] * z;
		f[15] += f[3] * x + f[7] * y + f[11] * z;
		return this;
	}

	public Matrix4f scale(float x, float y, float z) {
		f[0] *= x;
		f[4] *= x;
		f[8] *= x;
		f[12] *= x;
		f[1] *= y;
		f[5] *= y;
		f[9] *= y;
		f[13] *= y;
		f[2] *= z;
		f[6] *= z;
		f[10] *= z;
		f[14] *= z;
		return this;
	}

	public Matrix4f multiply(Matrix4f m) {
		float[] t = TEMPORARY().f;
		t[0] = f[0] * m.f[0] + f[1] * m.f[4] + f[2] * m.f[8] + f[3] * m.f[12];
		t[1] = f[0] * m.f[1] + f[1] * m.f[5] + f[2] * m.f[9] + f[3] * m.f[13];
		t[2] = f[0] * m.f[2] + f[1] * m.f[6] + f[2] * m.f[10] + f[3] * m.f[14];
		t[3] = f[0] * m.f[3] + f[1] * m.f[7] + f[2] * m.f[11] + f[3] * m.f[15];
		t[4] = f[4] * m.f[0] + f[5] * m.f[4] + f[6] * m.f[8] + f[7] * m.f[12];
		t[5] = f[4] * m.f[1] + f[5] * m.f[5] + f[6] * m.f[9] + f[7] * m.f[13];
		t[6] = f[4] * m.f[2] + f[5] * m.f[6] + f[6] * m.f[10] + f[7] * m.f[14];
		t[7] = f[4] * m.f[3] + f[5] * m.f[7] + f[6] * m.f[11] + f[7] * m.f[15];
		t[8] = f[8] * m.f[0] + f[9] * m.f[4] + f[10] * m.f[8] + f[11] * m.f[12];
		t[9] = f[8] * m.f[1] + f[9] * m.f[5] + f[10] * m.f[9] + f[11] * m.f[13];
		t[10] = f[8] * m.f[2] + f[9] * m.f[6] + f[10] * m.f[10] + f[11] * m.f[14];
		t[11] = f[8] * m.f[3] + f[9] * m.f[7] + f[10] * m.f[11] + f[11] * m.f[15];
		t[12] = f[12] * m.f[0] + f[13] * m.f[4] + f[14] * m.f[8] + f[15] * m.f[12];
		t[13] = f[12] * m.f[1] + f[13] * m.f[5] + f[14] * m.f[9] + f[15] * m.f[13];
		t[14] = f[12] * m.f[2] + f[13] * m.f[6] + f[14] * m.f[10] + f[15] * m.f[14];
		t[15] = f[12] * m.f[3] + f[13] * m.f[7] + f[14] * m.f[11] + f[15] * m.f[15];
		f[0] = t[0];
		f[1] = t[1];
		f[2] = t[2];
		f[3] = t[3];
		f[4] = t[4];
		f[5] = t[5];
		f[6] = t[6];
		f[7] = t[7];
		f[8] = t[8];
		f[9] = t[9];
		f[10] = t[10];
		f[11] = t[11];
		f[12] = t[12];
		f[13] = t[13];
		f[14] = t[14];
		f[15] = t[15];
		return this;
	}

	public Matrix4f rotate(float angle, float x, float y, float z) {
		float a = (float) Math.toRadians(angle);
		float cos = (float) Math.cos(a);
		float sin = (float) Math.sin(a);
		float mc = 1.0f - cos;
		float xy = x * y;
		float yz = y * z;
		float xz = x * z;
		float xs = x * sin;
		float ys = y * sin;
		float zs = z * sin;
		float f00 = x * x * mc + cos;
		float f10 = xy * mc + zs;
		float f20 = xz * mc - ys;
		float f01 = xy * mc - zs;
		float f11 = y * y * mc + cos;
		float f21 = yz * mc + xs;
		float f02 = xz * mc + ys;
		float f12 = yz * mc - xs;
		float f22 = z * z * mc + cos;
		float t00 = f[0] * f00 + f[1] * f10 + f[2] * f20;
		float t10 = f[4] * f00 + f[5] * f10 + f[6] * f20;
		float t20 = f[8] * f00 + f[9] * f10 + f[10] * f20;
		float t30 = f[12] * f00 + f[13] * f10 + f[14] * f20;
		float t01 = f[0] * f01 + f[1] * f11 + f[2] * f21;
		float t11 = f[4] * f01 + f[5] * f11 + f[6] * f21;
		float t21 = f[8] * f01 + f[9] * f11 + f[10] * f21;
		float t31 = f[12] * f01 + f[13] * f11 + f[14] * f21;
		f[2] = f[0] * f02 + f[1] * f12 + f[2] * f22;
		f[6] = f[4] * f02 + f[5] * f12 + f[6] * f22;
		f[10] = f[8] * f02 + f[9] * f12 + f[10] * f22;
		f[14] = f[12] * f02 + f[13] * f12 + f[14] * f22;
		f[0] = t00;
		f[4] = t10;
		f[8] = t20;
		f[12] = t30;
		f[1] = t01;
		f[5] = t11;
		f[9] = t21;
		f[13] = t31;
		return this;
	}

	public Matrix4f transpose() {
		float[] t = TEMPORARY().f;
		t[0] = f[0];
		t[4] = f[1];
		t[8] = f[2];
		t[12] = f[3];
		t[1] = f[4];
		t[5] = f[5];
		t[9] = f[6];
		t[13] = f[7];
		t[2] = f[8];
		t[6] = f[9];
		t[10] = f[10];
		t[14] = f[11];
		t[3] = f[12];
		t[7] = f[13];
		t[11] = f[14];
		t[15] = f[15];
		f[0] = t[0];
		f[1] = t[1];
		f[2] = t[2];
		f[3] = t[3];
		f[4] = t[4];
		f[5] = t[5];
		f[6] = t[6];
		f[7] = t[7];
		f[8] = t[8];
		f[9] = t[9];
		f[10] = t[10];
		f[11] = t[11];
		f[12] = t[12];
		f[13] = t[13];
		f[14] = t[14];
		f[15] = t[15];
		return this;
	}

	public Matrix4f createCopy() {
		return new Matrix4f(this.f);
	}

	public Matrix4f copyFrom(Matrix4f m) {
		for (int i = 0; i < 16; i++) f[i] = m.f[i];
		return this;
	}
}
