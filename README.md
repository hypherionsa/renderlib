# RenderLib

RenderLib is a small lightweight rendering engine written in pure Java and is highly optimized. Because of its high level of abstraction it should be simple to use.
The set of features it provides will be expanded in the future. Changes made partially depend on the feedback and requests for RenderLib.

If you are a developer or just a normal user and want to chat or make suggestions you can join the official discord server [here](https://discord.gg/44RENR4).

RenderLib uses the following additional libraries (packed into the distributed jar):
*  https://github.com/OpenHFT/Zero-Allocation-Hashing (licensed under Apache License 2.0 - https://www.apache.org/licenses/LICENSE-2.0)
*  https://github.com/xerial/snappy-java (licensed under Apache License 2.0 - https://www.apache.org/licenses/LICENSE-2.0)