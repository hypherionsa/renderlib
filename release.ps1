$answer = Read-Host -Prompt "Are you sure that you want to push a release? [y/N]"
if ($answer -eq "y") {
    .\gradlew uploadArchives
}
elseif ($answer -eq "N") {
    Write-Host "Aborting"
}
else {
    Write-Host "Aborting due to incorrect answer ($answer)"
}