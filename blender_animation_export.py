bl_info = {
    "name":         "Keyframe Animation Export",
    "author":       "FriedrichLP",
    "blender":      (2,7,9),
    "version":      (1,0,0),
    "location":     "File > Import-Export",
    "description":  "Export Keyframe Animation data",
    "category":     "Import-Export",
}

import bpy
import os
import os.path
from bpy_extras.io_utils import ExportHelper
from bpy.props import BoolProperty
        
class ExportAnim( bpy.types.Operator, ExportHelper ):
    bl_idname = "export.anim"
    bl_label = "Export Animation"

    filename_ext = ".anim"
    
    def invoke( self, context, event ):
        return ExportHelper.invoke( self, context, event )
    
    @classmethod
    def poll( cls, context ):
        return context.active_object != None
    
    def execute( self, context ):
        if not self.properties.filepath:
            raise Exception("filename not set")
        
        filepath = self.filepath

        if not filepath.lower().endswith(".anim"):
            filepath += ".anim"
            
        with open(filepath, "w") as file:
            file.write("#Animation file generated by AnimExport made by FriedrichLP\n")
            actions = None
            
            if self.only_selected == True:
                actions = bpy.data.actions[0]
            else:
                actions = bpy.data.actions
                
            for action in actions:
                    length = len(action.fcurves[0].keyframe_points)
                    curves = action.fcurves
                    if len(curves) != 9:
                        continue
                    file.write("o " + action.name.replace("Action", "") + "\n")
                    for i in range(0, length):
                        file.write("t " + str(curves[0].keyframe_points[i].co[0] / 30) + "\n")
                        file.write("l " + str(round(curves[0].keyframe_points[i].co[1], 6)) + " " + str(round(curves[1].keyframe_points[i].co[1], 6)) + " " + str(round(curves[2].keyframe_points[i].co[1], 6)) + "\n")
                        file.write("r " + str(round(curves[3].keyframe_points[i].co[1], 6)) + " " + str(round(curves[4].keyframe_points[i].co[1], 6)) + " " + str(round(curves[5].keyframe_points[i].co[1], 6)) + "\n")
                        file.write("s " + str(round(curves[6].keyframe_points[i].co[1], 6)) + " " + str(round(curves[7].keyframe_points[i].co[1], 6)) + " " + str(round(curves[8].keyframe_points[i].co[1], 6)) + "\n")
            
        return {"FINISHED"}
    
    only_selected = BoolProperty(name="Only selected object", default=False, description="Export only the Animation from selected object")
    
    def draw (self, context):
        layout = self.layout
        box = layout.box()
        
        box.label("Export Options:", icon="EXPORT")
        box.prop(self, "only_selected")
        box.row().active = bpy.data.is_saved
        

def menu_func_export( self, context ):
    self.layout.operator( ExportAnim.bl_idname, text="Animation (.anim)")
    
def register():
    bpy.utils.register_class(ExportAnim)
    bpy.types.INFO_MT_file_export.append(menu_func_export)

def unregister():
    bpy.utils.unregister_class(ExportAnim)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
    register()